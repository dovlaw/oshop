import { map } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private db: AngularFireDatabase) { }

  getAll() {
    return this.db.list('/categories',  cat => cat.orderByChild('name')).snapshotChanges().map(actions => {
      return actions.map(action => ({key: action.key, ...action.payload.val() as {}}));
    });
  }

}
