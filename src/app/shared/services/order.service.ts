import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  lastOrder;
  lastOrderItems;
  constructor(private db: AngularFireDatabase, private shoppingCartService: ShoppingCartService) { }
  
  async placeOrder(order) {
    let result = await this.db.list('/orders').push(order);
    this.shoppingCartService.clearCart();
    return result;
  }

  getOrders() {
    return this.db.list('/orders').snapshotChanges().map(actions => {
      return actions.map(action => ({key: action.key, ...action.payload.val() as {}}));
    });
  }

  getOrdersByUser(userId: string) {
    return this.db.list('/orders', ref => ref.orderByChild('userId').equalTo(userId)).valueChanges();
  }

  getLastOrder() {
    return this.lastOrder = this.db.list('/orders', ref => ref.limitToLast(1)).valueChanges();
  }

  getLastOrderItems() {
  return this.lastOrderItems = this.db.list('/orders', ref => ref.limitToLast(1).orderByChild('items')).valueChanges();
  }

}
