import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { I18nModule } from 'app/translate/i18n.module';
import { CustomFormsModule } from 'ng2-validation';
import { AuthGuardService as AuthGuard } from 'shared/services/auth-guard.service';
import { AuthService } from 'shared/services/auth.service';

import { ProductCardComponent } from './components/product-card/product-card.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductQuantityComponent } from './components/product-quantity/product-quantity.component';
import { CategoryService } from './services/category.service';
import { ImageService } from './services/image.service';
import { OrderService } from './services/order.service';
import { ProductService } from './services/product.service';
import { ShoppingCartService } from './services/shopping-cart.service';
import { UserService } from './services/user.service';



@NgModule({
  declarations: [
    ProductCardComponent,
    ProductQuantityComponent,
    ProductDetailsComponent,
  ],
  exports: [
    CommonModule,
    ProductCardComponent,
    ProductQuantityComponent,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FontAwesomeModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    UserService,
    CategoryService,
    ProductService,
    ShoppingCartService,
    OrderService,
    ImageService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CustomFormsModule,
    NgbModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    NgbModule,
    FontAwesomeModule,
    I18nModule,
    RouterModule.forChild([
      { path: 'product-details/:id', component: ProductDetailsComponent }
    ])
  ]
})
export class SharedModule { }
