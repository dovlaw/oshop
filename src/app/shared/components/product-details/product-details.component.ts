import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from './../../services/product.service';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Product } from 'shared/models/product';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent {
  @Input('product') product: Product;
  @Input('show-actions') showActions = true;
  @Input('shopping-cart') shoppingCart: ShoppingCart;

  id: string;

  constructor(
    private cartService: ShoppingCartService, 
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute,) {

      this.id = this.route.snapshot.paramMap.get('id');
      if (this.id) this.productService.get(this.id).pipe(take(1)).subscribe((p: Product) => this.product = p);
    }

  addToCart() {
    this.cartService.addToCart(this.product);
  }
}
