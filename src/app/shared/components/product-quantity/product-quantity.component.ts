import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'shared/models/product';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';

@Component({
  selector: 'product-quantity',
  templateUrl: './product-quantity.component.html',
  styleUrls: ['./product-quantity.component.css']
})
export class ProductQuantityComponent {
  @Input('product') product: Product;
  @Input('shopping-cart') shoppingCart;

  constructor(private cartService: ShoppingCartService) { }

  addToCart() {
    this.cartService.addToCart(this.product);
  }

  addToCart10() {
    this.cartService.addToCart10(this.product);
  }

  removeFromCart() {
    this.cartService.removeFromCart(this.product);

  }

  removeFromCart10() {
    this.cartService.removeFromCart10(this.product);
  }

}
