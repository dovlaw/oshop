import { TranslateService } from '@ngx-translate/core';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { Product } from 'shared/models/product';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {
  public products = [];
  @Input('product') product: Product;
  @Input('show-actions') showActions = true;
  @Input('shopping-cart') shoppingCart: ShoppingCart;

  constructor(private cartService: ShoppingCartService, private translate: TranslateService) {
    translate.addLangs(['en', 'sr']);
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|sr/) ? browserLang : 'en');
    // loop for build a product object and push to the array
    for (let index = 1; index <= 19; index++) {
      const productObj = {
        id: index,
        product: 'product_' + index.toString(),
        description: 'product.description_' + index.toString()
      };
      this.products.push(productObj);
    }
  
  }

  addToCart() {
    this.cartService.addToCart(this.product);
  }

}
