import { Component, OnInit } from '@angular/core';
import { OrderService } from './../../../shared/services/order.service';

@Component({
  selector: 'app-order-success',
  templateUrl: './order-success.component.html',
  styleUrls: ['./order-success.component.css']
})
export class OrderSuccessComponent implements OnInit {
  order$;
  items$;
  constructor(private orderService: OrderService) {  }

  ngOnInit() {
    this.order$ = this.orderService.getLastOrder();
    this.items$ = this.orderService.getLastOrderItems();

  }
}
