import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdminGalleryComponent } from 'app/admin/components/admin-images/admin-gallery/admin-gallery.component';
import { SelectLanguageComponent } from 'app/translate/components/select-language/select-language.component';
import { I18nModule } from 'app/translate/i18n.module';

import { AdminModule } from './../admin/admin.module';
import { SharedModule } from './../shared/shared.module';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { BsNavbarComponent } from './components/bs-navbar/bs-navbar.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';



@NgModule({
  declarations: [
    BsNavbarComponent,
    HomeComponent,
    LoginComponent,
    AboutUsComponent,
    ContactUsComponent,
    SelectLanguageComponent
  ],
  exports: [
    BsNavbarComponent,
    
  ],
  imports: [
    SharedModule,
    FormsModule,
    AdminModule,
    I18nModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'home', component: HomeComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: 'gallery', component: AdminGalleryComponent },
      { path: 'contact-us', component: ContactUsComponent },
    ])
  ]
})
export class CoreModule { }
