import { Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateLoader } from '@ngx-translate/core';
import { Component} from '@angular/core';

@Component({
  selector: 'app-firebase-trans-loader',
  template: `
    <p>
      firebase-trans-loader works!
    </p>
  `,
  styles: [
  ]
})
export class FirebaseTransLoaderComponent implements TranslateLoader {

  constructor(private db: AngularFireDatabase, private prefix:string = 'translations/') { }
  public getTranslation(lang: string): any {
    return this.db.object('${this.prefix}${lang}') as unknown as Observable<any>;
  }

}
