import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-language',
  template: `
  <select #langSelect (change)="translate.use(langSelect.value)"class="dropdown">
    <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">Dropdown Example
    <span class="caret"></span></button>
    <option
       *ngFor="let lang of translate.getLangs()"
       [value]="lang"
       [attr.selected]="lang === translate.currentLang ? '' : null"
    >{{lang}}</option>
  </select>

  `,
  styles: [
  ]
})
export class SelectLanguageComponent {

  constructor(public translate: TranslateService) { }

}
