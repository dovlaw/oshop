import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'shared/services/auth-guard.service';

import { SharedModule } from './../shared/shared.module';
import { AdminGalleryComponent } from './components/admin-images/admin-gallery/admin-gallery.component';
import { AdminImageComponent } from './components/admin-images/admin-image.component';
import { AdminUploadComponent } from './components/admin-images/admin-upload/admin-upload.component';
import { AdminOrdersComponent } from './components/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { AdminAuthGuardService as AdminAuthGuard } from './services/admin-auth-guard.service';


@NgModule({
  declarations: [
    AdminProductsComponent,
    AdminOrdersComponent,
    ProductFormComponent,
    AdminImageComponent,
    AdminUploadComponent,
    AdminGalleryComponent
  ],
  providers: [
    AdminAuthGuard
  ],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { 
        path: 'admin/products/new', 
        component: ProductFormComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
      { 
        path: 'admin/products/:id', 
        component: ProductFormComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
      { 
        path: 'admin/products', 
        component: AdminProductsComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
      { 
        path: 'admin/orders', 
        component: AdminOrdersComponent, 
        canActivate: [AuthGuard, AdminAuthGuard] 
      },
      { 
        path: 'admin/images', 
        component: AdminImageComponent, 
        canActivate: [AuthGuard, AdminAuthGuard],
        children: [
          {path: 'upload', component: AdminUploadComponent},
          {path: 'gallery', component: AdminGalleryComponent}
          
        ]
      },
      { path: 'admin/images', redirectTo: 'admin/upload', pathMatch: 'full'},
    ]),
  ]
})
export class AdminModule { }
