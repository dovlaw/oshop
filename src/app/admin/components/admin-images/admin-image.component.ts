import { Component, OnInit } from '@angular/core';
import { ImageService } from 'shared/services/image.service';

@Component({
  selector: 'admin-images',
  templateUrl: './admin-image.component.html',
  styleUrls: ['./admin-image.component.css']
})
export class AdminImageComponent implements OnInit {

  constructor( private imageService: ImageService) { }

  ngOnInit(): void {
    this.imageService.getImageDetailList();
  }
}
