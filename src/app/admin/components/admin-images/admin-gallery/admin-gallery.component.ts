import { ImageService } from './../../../../shared/services/image.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'admin-gallery',
  templateUrl: './admin-gallery.component.html',
  styleUrls: ['./admin-gallery.component.css']
})
export class AdminGalleryComponent implements OnInit {
  imageList: any[];
  rowIndexArray: any[];

  constructor(private imageService: ImageService) { }

  ngOnInit(): void {
    
    this.imageService.imageDetailList?.snapshotChanges().subscribe(list => {
      this.imageList = list.map(item => {return item.payload.val();});
      this.rowIndexArray = Array.from(Array(Math.ceil(this.imageList.length / 3)).keys());
    });
  }

}
