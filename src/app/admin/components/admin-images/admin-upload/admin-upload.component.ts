import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { ImageService } from 'shared/services/image.service';

@Component({
  selector: 'admin-images',
  templateUrl: './admin-upload.component.html',
  styleUrls: ['./admin-upload.component.css']
})
export class AdminUploadComponent implements OnInit {
  imageSrc: string;
  selectedImg: any;
  isSubmitted: boolean;
  

  formTemplate = new FormGroup({
    caption: new FormControl('', Validators.required), 
    category: new FormControl('', Validators.required),
    imageUrl: new FormControl('', Validators.required),
  }) 

  constructor(private storage: AngularFireStorage, private imageService: ImageService) { }

  ngOnInit(): void {
    this.resetForms();
  }

  showPreview(event: any){
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imageSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
    }else{
      this.imageSrc =  '/assets/img/Placeholder.jpg';
      this.selectedImg = null;
    } 
  }

  onSubmit(formValue){
    this.isSubmitted = true;
    if (this.formTemplate.valid) {
      var filePath = `${formValue.category}/${this.selectedImg.name}_${new Date().getTime()}`;
      const fileRef = this.storage.ref(filePath);
      this.storage.upload(filePath, this.selectedImg).snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            formValue['imageUrl'] = url;
            this.imageService.insertImageDetails(formValue);
            this.resetForms();
          })
        })
      ).subscribe();
    }
  }

  get formControls(){
    return this.formTemplate['controls'];
  }

  resetForms(){
    this.formTemplate.reset();
    this.formTemplate.setValue({
      caption: '',
      imageUrl: '',
      category: 'Bath robes'
    });
    this.imageSrc = '/assets/img/Placeholder.jpg';
    this.selectedImg  = null;
    this,this.isSubmitted  = false;

  }
}
