// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAOxmzfQFFZ0JBd4zlHaOpdEwDx4tSwU20",
    authDomain: "oshop-b8d1a.firebaseapp.com",
    databaseURL: "https://oshop-b8d1a.firebaseio.com",
    projectId: "oshop-b8d1a",
    storageBucket: "oshop-b8d1a.appspot.com",
    messagingSenderId: "363846564320",
    appId: "1:363846564320:web:141ccdd3f384b21229d385",
    measurementId: "G-2HN65V1DJ6"

  }

};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
